<?xml version="1.0" encoding="UTF-8"?>
<vmap:VMAP xmlns:vmap="http://www.iab.net/videosuite/vmap" version="1.0">
	{{#each adBreaks }}
	<vmap:AdBreak timeOffset="{{timeOffset}}" breakType="{{breakType}}" breakId="{{breakId}}">
	
		{{#each instreamVideoAds}}
		<vmap:AdSource id="{{adId}}" allowMultipleAds="false" followRedirects="true">
		<vmap:VASTAdData>
			<VAST xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="vast.xsd" version="3.0">
			   <Ad id="{{adId}}">
			      <InLine>
			         <AdSystem version="1.0">AdsPLAY</AdSystem>
			         <AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
			         <Impression><![CDATA[{{logUrl}}?event=impression&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Impression>
			         {{#each impression3rdUrls}}
			         <Impression><![CDATA[{{this}}]]></Impression>
			         {{/each}}
			         <Creatives>
			            <Creative sequence="1" id="{{adId}}">
			               <Linear skipoffset="{{skipoffset}}">
			                  <Duration>{{duration}}</Duration>
			                  <TrackingEvents>
			                     {{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?event=creativeView&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?event=start&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?event=view25&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?event=view50&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?event=view75&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>								 
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?event=view100&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?event=close&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
			                  </TrackingEvents>
			                  <VideoClicks>
			                     {{videoClicksTags}}
			                     <ClickTracking><![CDATA[{{logUrl}}?event=click&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></ClickTracking>
			                  </VideoClicks>
			                  <MediaFiles>
			                     <MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
			                  </MediaFiles>
			               </Linear>
			            </Creative>
			         </Creatives>
			      </InLine>
			   </Ad>
			</VAST>
		</vmap:VASTAdData>
		</vmap:AdSource>
		{{/each}}
	
	</vmap:AdBreak>
	{{/each}}
 </vmap:VMAP>