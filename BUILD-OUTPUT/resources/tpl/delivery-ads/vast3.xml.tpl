<?xml version="1.0" encoding="UTF-8"?>
<VAST xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="vast.xsd" version="3.0">
	<Ad id="{{id}}">
		<InLine>			
			<AdSystem version="1.0">AdsPLAY</AdSystem>
			<AdTitle>AdsPLAY_TVC</AdTitle>
			<Description>AdsPLAY_Mobile</Description>							
			<Creatives>			
				{{#each instreamVideoAds}}												
				<Creative id="crt-{{adId}}" sequence="1">
					<Linear skipoffset="{{skipoffset}}">
						<Duration>{{duration}}</Duration>
						<TrackingEvents>
							<Tracking event="start"><![CDATA[{{logUrl}}?event=impression&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							{{#each impression3rdUrls}}
								<Tracking event="start"><![CDATA[{{this}}]]></Tracking>
							{{/each}}							
							<Tracking event="complete"><![CDATA[{{logUrl}}?event=view100&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							<Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?event=view75&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							<Tracking event="midpoint"><![CDATA[{{logUrl}}?event=view50&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							<Tracking event="firstQuartile"><![CDATA[{{logUrl}}?event=view25&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
															
							<Tracking event="fullscreen"><![CDATA[{{logUrl}}?event=fullscreen&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							<Tracking event="close"><![CDATA[{{logUrl}}?event=close&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
							<Tracking event="skip"><![CDATA[{{logUrl}}?event=skip&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
														
							{{trackingEvent3rdTags}}
						</TrackingEvents>
						<VideoClicks>
						<ClickThrough><![CDATA[http://d4.adsplay.net/click-redirect?adid={{adId}}&placement={{placementId}}&uuid={{uuid}}&url={{encodedClickThrough}}]]></ClickThrough>
						</VideoClicks>
						<MediaFiles><MediaFile height="360" width="640" delivery="progressive" type="video/mp4" bitrate="116" scalable="true" maintainAspectRatio="true"><![CDATA[{{mediaFile}}]]></MediaFile></MediaFiles>
					</Linear>
				</Creative>				
				{{/each}}								
			</Creatives>
		</InLine>
	</Ad>
</VAST>