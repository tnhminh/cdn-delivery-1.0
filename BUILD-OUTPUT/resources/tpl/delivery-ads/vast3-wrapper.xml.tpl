<?xml version="1.0" encoding="UTF-8"?>
<VAST version="3.0">

	{{#each instreamVideoAds}}
		<Ad id="{{adId}}">
			   {{#if vastAdTagURI}}
			      <Wrapper fallbackOnNoAd="true">
			   {{/if}}
			   {{#unless vastAdTagURI}}
			      <InLine>
			   {{/unless}}
				<AdSystem version="1.0">AdsPLAY</AdSystem>
				<AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
				<Description>
							<![CDATA[{{description}}]]>
				</Description>
				 {{#if vastAdTagURI}}
			         	<VASTAdTagURI><![CDATA[{{vastAdTagURI}}]]></VASTAdTagURI>
			     {{/if}}
				  <Error>
					  <![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=error&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&errorCode=[ERRORCODE]&isFallbackAd={{isFallbackAd}}]]>
				 </Error>
				 
				   <Error>
						<![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]>
					 </Error>
					 
			         <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=impression&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Impression>
			         
			         
			        <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Impression>

			         
				{{#each impression3rdUrls}}
					<Impression><![CDATA[{{this}}]]></Impression>
				{{/each}}

				<Creatives>
					<Creative sequence="1" id="{{adId}}">						
						<Linear skipoffset="{{skipoffset}}" starttime="{{startTime}}" >
							<Duration>{{duration}}</Duration>
							<TrackingEvents>
								{{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=creativeView&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played0&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played25&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played50&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played75&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played100&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=close&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                  	 <Tracking event="pause"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=paused&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                  	 <Tracking event="mute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=mute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
			                  	 <Tracking event="unmute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=unmute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
								 <Tracking event="skip"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=skip&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Tracking>
							</TrackingEvents>
							
							 {{#if vastAdTagURI}}
				                  <VideoClicks>
				                  {{videoClicksTags}}
				                  <ClickTracking><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=click&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></ClickTracking>
				                  </VideoClicks>
			                  {{/if}}
			                  
				              {{#unless vastAdTagURI}}
				                  <VideoClicks>
				                     {{videoClicksTags}}
				                     <ClickTracking><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=click&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></ClickTracking>
				                  </VideoClicks>
				                  
				                  <MediaFiles>
				                  	{{#each mediaFiles as |mediaFile|}}
				                     <MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				                  	{{/each}}
				                  	
				                  	{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
				                  </MediaFiles>

			                  {{/unless}}
							
						</Linear>
					</Creative>	
				</Creatives>
				<Extensions>
						<Extension type="waterfall" fallback_index="0"/>
				</Extensions>		
			{{#if vastAdTagURI}}
			      </Wrapper>
			   {{/if}}
			   {{#unless vastAdTagURI}}
			      </InLine>
			   {{/unless}}
		</Ad>
		<Ad id="fallback_{{passbackData}}">
						<Wrapper>
							<AdSystem version="1.0">AdsPLAY</AdSystem>
							<AdTitle>
								<![CDATA[ AdsFallback]]>
							</AdTitle>
							<Description>
								<![CDATA[ AdsFallback_run]]>
							</Description>
							<VASTAdTagURI>
								<![CDATA[
{{baseUrl}}/delivery?pdata={{passbackData}}&url={{pageUrl}}&adFallBack=1&thirdptName=Falback-Ads
]]>
							</VASTAdTagURI>
						
							<Extensions>
								<Extension type="waterfall" fallback_index="1"/>
							</Extensions>
						</Wrapper>
					</Ad>
	{{/each}}		
	
</VAST>
