<?xml version="1.0" encoding="UTF-8"?>
<vmap:VMAP xmlns:vmap="http://www.iab.net/videosuite/vmap" version="1.0">
	{{#each adBreaks }}
	 <vmap:AdBreak timeOffset="{{timeOffset}}" breakType="{{breakType}}" breakId="{{breakId}}">
	  <vmap:AdSource id="preroll-ad-1" allowMultipleAds="false" followRedirects="true">
	   <vmap:AdTagURI templateType="vast3"><![CDATA[{{adTagURI}}]]></vmap:AdTagURI>
	  </vmap:AdSource>
	 </vmap:AdBreak>
	{{/each}}	
 </vmap:VMAP>