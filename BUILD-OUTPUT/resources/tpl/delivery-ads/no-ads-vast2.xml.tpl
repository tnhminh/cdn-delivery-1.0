<?xml version="1.0" encoding="utf-8" ?>
<VAST version="3.0">
  <Ad id="{{adId}}">
	<InLine>
		<Error>
			<![CDATA[ {{domain}}/tracking/v2?evt=nodata&{{url}}]]>
		</Error>
		
		<Error>
			<![CDATA[ {{domain}}/tracking/v2?evt=request&{{url}}]]>
		</Error>
	</InLine>
  </Ad>
</VAST>
