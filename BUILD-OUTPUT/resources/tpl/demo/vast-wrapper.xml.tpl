<VideoAdServingTemplate>
    <Ad id="videoAd_wrapper">
        <Wrapper>
            <AdSystem>AdServer</AdSystem>
            <VASTAdTagURL>
                <URL <![CDATA[https://d2.adsplay.net/delivery?placement=302]]> </URL>
            </VASTAdTagURL>
            <Impression><![CDATA[https://imp.adnetwork.vn/247/adServer/act_impression/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/imp_ijew3t88tpl/sid_zworynee1u0/cb_769738633]]></Impression>
	   <TrackingEvents>	                            
            <Tracking event="start"><![CDATA[https://metrics.adnetwork.vn/247/adServer/act_video_metric/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/vlen_15/vtype_abdStartPlayed/imp_ijew3t88tpl/cb_769738633/]]></Tracking>
        
            <Tracking event="firstQuartile"><![CDATA[https://metrics.adnetwork.vn/247/adServer/act_video_metric/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/vlen_15/vtype_abdPlayed25/imp_ijew3t88tpl/cb_769738633/]]></Tracking>
        
            <Tracking event="midpoint"><![CDATA[https://metrics.adnetwork.vn/247/adServer/act_video_metric/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/vlen_15/vtype_abdPlayed50/imp_ijew3t88tpl/cb_769738633/]]></Tracking>
        
            <Tracking event="thirdQuartile"><![CDATA[https://metrics.adnetwork.vn/247/adServer/act_video_metric/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/vlen_15/vtype_abdPlayed75/imp_ijew3t88tpl/cb_769738633/]]></Tracking>
        
            <Tracking event="complete"><![CDATA[https://metrics.adnetwork.vn/247/adServer/act_video_metric/cid_1469587983/bid_1469600440/pid_1335342353/wid_1432262118/zid_1432262200/vlen_15/vtype_abdFullyPlayed/imp_ijew3t88tpl/cb_769738633/]]></Tracking>
	   </TrackingEvents>
        </Wrapper>
    </Ad>
</VideoAdServingTemplate>