package adsplay.common.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

public class HashUtils {

    public static String hashMD5(String filename) {
        HashCode hash;
        try {
            hash = com.google.common.io.Files.hash(new File(filename), Hashing.md5());
            return hash.toString().toUpperCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public static String hashMD5String(String name) {
        return DigestUtils.md5Hex(name).toUpperCase();
    }
}
