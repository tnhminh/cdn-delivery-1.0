package adsplay.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ContainerUtils {

    public static String getName() {
        String hostName = "";
        try {
            hostName = InetAddress.getLocalHost().getHostAddress() + "_" + InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return hostName;
    }

}
