package adsplay.common.util;

import java.util.ArrayList;
import java.util.List;

public class VinSmartTVAllow {
    
    private static final List<String> ALLOW_PLACEMENT_IDS = new ArrayList<String>();
    static {
    }
    
    public static boolean isNotAllowVinSmartTV(String userAgent, String ipAddress, List<String> pmIds) {
        return (userAgent.contains("VINATV") || userAgent.contains("vinatv")) && !ALLOW_PLACEMENT_IDS.containsAll(pmIds);
    }
}
