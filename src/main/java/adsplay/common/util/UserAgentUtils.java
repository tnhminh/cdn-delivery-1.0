package adsplay.common.util;

public class UserAgentUtils {

    private static String getUaIOS(String ua) {
        try {
            ua = ua.replaceFirst("\\[", "HaiBegin[").replaceFirst("\\]", "]HaiEnd");
            return subString(ua);
        } catch (Exception e) {
            return ua;
        }
    }

    private static String getUaAndroid(String ua) {
        try {
            ua = ua.replaceFirst("\\(", "HaiBegin(").replaceFirst("\\)", ")HaiEnd");
            return subString(ua);
        } catch (Exception e) {
            return ua;
        }
    }

    private static String subString(String ua) {
        int startIndex = ua.indexOf("HaiBegin");
        if (startIndex > 0) {
            try {
                return ua.substring(ua.indexOf("HaiBegin") + 8, ua.indexOf("HaiEnd"));
            } catch (Exception e) {
                return ua.substring(ua.indexOf("HaiBegin") + 8);
            }
        }
        return ua;
    }

    public static String prepareUa(String rawUa) {
        if (rawUa.contains("Mozilla")) {
            if (rawUa.contains("Mac OS X") && !rawUa.contains("Macintosh")) {
                return getUaIOS(rawUa);
            } else {
                return getUaAndroid(rawUa);
            }
        }
        return rawUa;
    }
}
