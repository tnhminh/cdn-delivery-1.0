package adsplay.common.util;

import rfx.core.util.CharPool;
import rfx.core.util.StringPool;

public class AdData extends LogData {

    String metric;
    int creativeId;
    int campaignId;
    int flightId;
    int timeEngaged = 1;//the time when user get Vast Ad Info to metric event, default is 1 second

    public AdData(String metric, int loggedTime, String uuid, int placement, int creativeId,
                  int campaignId, int flightId) {
        super();
        this.metric = metric;
        this.loggedTime = loggedTime;
        this.uuid = uuid;
        this.placement = placement;
        this.creativeId = creativeId;
        this.campaignId = campaignId;
        this.flightId = flightId;
    }

    public String getMetric() {
        return metric;
    }

    public int getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(int creativeId) {
        //TODO set and check creativeId
        if (this.creativeId == 0) {
            this.creativeId = creativeId;
        }
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getTimeEngaged() {
        return timeEngaged;
    }

    public void setTimeEngaged(int timeEngaged) {
        this.timeEngaged = timeEngaged;
    }

    @Override
    String toLogRecordSplitByTab() {
        StringBuilder s = new StringBuilder();

        // event time
        char tab = CharPool.TAB;
        s.append(this.loggedTime).append(tab);

        // advertising data
        s.append(this.metric).append(tab);
        s.append(this.timeEngaged).append(tab);
        s.append(this.placement).append(tab);
        s.append(this.creativeId).append(tab);
        s.append(this.campaignId).append(tab);
        s.append(this.flightId).append(tab);

        // context logs
        s.append(this.ip).append(tab);
        s.append(this.uuid).append(tab);
        s.append(this.url).append(tab);
        s.append(this.refererUrl).append(tab);
        s.append(this.city).append(tab);
        s.append(this.country).append(tab);
        s.append(this.deviceType).append(tab);
        s.append(this.deviceName).append(tab);
        s.append(this.deviceOs).append(tab);
        s.append(this.userAgent);
        s.append(StringPool.NEW_LINE);
        return s.toString();
    }


}
