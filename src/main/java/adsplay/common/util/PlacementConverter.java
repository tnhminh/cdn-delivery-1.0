package adsplay.common.util;

import java.util.HashMap;
import java.util.Map;

import rfx.core.util.StringUtil;

public class PlacementConverter {

    private static final Map<String, String> PLACEMENT_ID_TO_REMOTE_FOLDER = new HashMap<String, String>();
    static {
        // List of placement id to remote folder name - mp4
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("202", "ctv");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("302", "mobile");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("102", "web");
        
        // Outstream - webm
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("11180407", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("11180307", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("1911070517", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("1911071116", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("1911080517", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("1911050517", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("1911060517", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("11051103", "outstream");
        PLACEMENT_ID_TO_REMOTE_FOLDER.put("11041103", "outstream");
    }

    public static String convert(String placementID) {
        String result = StringUtil.safeString(PLACEMENT_ID_TO_REMOTE_FOLDER.get(placementID), "");
        return result == null || result.isEmpty() ? "other" : result;
    }
}
