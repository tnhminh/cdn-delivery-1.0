package adsplay.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author minhtnh
 *
 */
public class PlacementUtils {

    public static List<Integer> BANNER_PLACEMENTS = new ArrayList<Integer>();
    static {
        BANNER_PLACEMENTS.add(105);
        BANNER_PLACEMENTS.add(107);
        BANNER_PLACEMENTS.add(111);
        BANNER_PLACEMENTS.add(112);
        BANNER_PLACEMENTS.add(106);
        BANNER_PLACEMENTS.add(401);
        BANNER_PLACEMENTS.add(104);
        BANNER_PLACEMENTS.add(109);
        BANNER_PLACEMENTS.add(113);
        BANNER_PLACEMENTS.add(114);
        BANNER_PLACEMENTS.add(115);
        BANNER_PLACEMENTS.add(117);
        BANNER_PLACEMENTS.add(118);
        BANNER_PLACEMENTS.add(119);
        BANNER_PLACEMENTS.add(120);
        BANNER_PLACEMENTS.add(121);
    }
    
    public static final Map<Integer, Integer> PM_WEBAPP_MAPPING = new HashMap<Integer, Integer>();
    static {
        PM_WEBAPP_MAPPING.put(104, 17);
    }
}
