package adsplay.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import adsplay.delivery.properties.MessageProperties;

public class ParamUtils {
    
    private static final String WEIGHT_CREATIVES_KEYS = "weight.creatives.key";

    private static final String LIVETV_TIME_PUSH_EVENT = "livetv.time.push.event";

    private static final String LIVETV_CHANNEL = "livetv.channel";

    public static final String PLACEMENT_ID = "placementId";

    private static final String PARAM_PUBLISHER_ID = "param.publisher_id";

    private static final String PARAM_UUID = "param.uuid";

    private static final String PARAM_CID = "param.cid";

    private static final String PARAM_CONTENT_ID = "param.content_id";

    private static final String PARAM_LOCATION = "param.location";

    private static final String PARAM_PAGE_URL = "param.page_url";

    private static final String PARAM_PARENT_CATEGORY = "param.parent_category";

    private static final String PARAM_CATEGORY = "param.category";

    private static final String PARAM_COPYRIGHT_ID = "param.copyright_id";

    private static final String PARAM_CON_ID = "param.con_id";

    private static final String PARAM_IS_PAYTV = "param.is_paytv";

    private static final String PARAM_MOVIE_ID = "param.movie_id";

    public static final String MOVIE_ID = MessageProperties.getProperty(PARAM_MOVIE_ID, "");
    
    public static final String CUSTOMER_ID = "customerID";

    public static final String IS_PAYTV = MessageProperties.getProperty(PARAM_IS_PAYTV, "");

    public static final String CON_ID = MessageProperties.getProperty(PARAM_CON_ID, "");

    public static final String COPYRIGHT_ID = MessageProperties.getProperty(PARAM_COPYRIGHT_ID, "");

    public static final String CATEGORY = MessageProperties.getProperty(PARAM_CATEGORY, "");
    
    public static final String PARENT_CATEGORY = MessageProperties.getProperty(PARAM_PARENT_CATEGORY, "");

    public static final String PAGE_URL = MessageProperties.getProperty(PARAM_PAGE_URL, "");

    public static final String LOCATION = MessageProperties.getProperty(PARAM_LOCATION, "");

    public static final String CONTENT_ID = MessageProperties.getProperty(PARAM_CONTENT_ID, "");

    public static final String CID = MessageProperties.getProperty(PARAM_CID, "");
    
    public static final String UUID = MessageProperties.getProperty(PARAM_UUID, "");
    
    public static final String PUBLISHER_ID = MessageProperties.getProperty(PARAM_PUBLISHER_ID, "");
    
    public static final String CHANNEL = MessageProperties.getProperty(LIVETV_CHANNEL, "");
    
    public static final String TIME_PUSH_EVENT = MessageProperties.getProperty(LIVETV_TIME_PUSH_EVENT, "");
    
    public static final String AD_TYPE = "ad_type";
    
    public static final String WEIGHT_CREATIVES_KEY = MessageProperties.getProperty(WEIGHT_CREATIVES_KEYS, "");
    
    public static final String EPISODE = "episode";
    
    public static Map<String, String> splitQuery(String url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url;
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

}
