package adsplay.common.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.provider.sftp.IdentityInfo;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import com.adsplay.cdn.trancoded.CdnHandler;
import com.adsplay.cdn.trancoded.CdnTrancodedJob;
import com.google.gson.GsonBuilder;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import adsplay.delivery.properties.DeliveryConfiguration;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import rfx.core.util.LogUtil;

public class UploadUtils {

    private static final String DOMAIN = DeliveryConfiguration.getProperty("delivery.cdn.trancoded.domain");
    private static final String HTTPS = "https";
    private static final String FULL_CDN_DOMAIN = HTTPS + "://" + DOMAIN ;
    private static final String TRANCODE_MEDIA_DIRECTORY = "trancode-media";
    private static final String ROOT_DIRECTORY = "/vod/";

    public static String startUploadVFS(CdnTrancodedJob job) throws IOException, URISyntaxException {

        // Login info
        String username = "hungnt";
        String remoteHost = "118.68.168.59";

        Map<String, Object> paramAsMapsJobCdn = new HashMap<String, Object>(job.getParamsAsmap());
        
        FileSystemManager manager = VFS.getManager();

        // Download 3pt cdn to local
        String cdnUrl = job.getUrlCdn();
        String fileType = cdnUrl.substring(cdnUrl.lastIndexOf(".") + 1);

        String fileName = HashUtils.hashMD5String(cdnUrl);

        String localPathname = "/home/hungnt/tnhminh/AdsProject/Adsplay_Ver2/FTEL-BACKEND/cdn-delivery-1.0/resources/cdn-local/" + fileName
                + "." + fileType;

        File localFile = new File(localPathname);
        
        boolean isLocalFileExists = localFile.exists();
        paramAsMapsJobCdn.put("isLocalFileExists", isLocalFileExists);
        
        if (!isLocalFileExists){
            FileUtils.copyURLToFile(new URL(cdnUrl), localFile);
            paramAsMapsJobCdn.put("copyURLToFile", "done");
        } 
        
        // Create Local and Remote end point
        FileObject local = manager.resolveFile(localPathname);

        String remoteDir = job.getRemoteDir();
        String destinationPath = ROOT_DIRECTORY + TRANCODE_MEDIA_DIRECTORY + "/" + remoteDir;
        String remoteSftpPathFile = "sftp://" + username + "@" + remoteHost + destinationPath + "/" + fileName + "."
                + fileType;
        FileSystemOptions createDefaultOptions = createDefaultOptions("/home/hungnt/.ssh/backup_key_minh/id_rsa", "");
        SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
        builder.getKnownHosts(createDefaultOptions);
        builder.setUserDirIsRoot(createDefaultOptions, false);
        builder.setTimeout(createDefaultOptions, 5000);

        FileObject remote = manager.resolveFile(remoteSftpPathFile, createDefaultOptions);

        String remoteCDN = FULL_CDN_DOMAIN + destinationPath + "/" + fileName + "." + fileType;

        if (!remote.exists()) {
            // Execute uploading
            remote.copyFrom(local, Selectors.SELECT_SELF);
            paramAsMapsJobCdn.put("uploading...", "done");
        }

        // Create cache mapping here
        // key = cdnUrl | value = remoteCDN
        RedisCache.hset("cdn_trancoded_mapping", job.getPlacementId() + "-" + cdnUrl, remoteCDN, RedisPool.CDN_CACHE);
        paramAsMapsJobCdn.put("setMappingKey...", "done");
        
        // Remove file after uploading successfully or remove after day
        File removedFile = localFile;
        removedFile.delete();
        paramAsMapsJobCdn.put("removed...", "done");
        
        // Close all connection
        local.close();
        remote.close();

        LogUtil.i(CdnHandler.class, "[Detailed-CDN-Job-Log]-" + new Date() + "-" + new GsonBuilder().create().toJson(paramAsMapsJobCdn), true);
        
        return remoteCDN;
    }
    
    private static FileSystemOptions createDefaultOptions(final String keyPath, final String passphrase) throws FileSystemException{

        //create options for sftp
        FileSystemOptions options = new FileSystemOptions();
        //ssh key
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(options, "no");
        //set root directory to user home
        SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(options, true);
        //timeout
        SftpFileSystemConfigBuilder.getInstance().setTimeout(options, 10000);

        if (keyPath != null) {
            IdentityInfo identityInfo = null;
            identityInfo = new IdentityInfo(new File(keyPath));
            SftpFileSystemConfigBuilder.getInstance().setIdentityInfo(options, identityInfo);
        }


        return options;
    }
    
    public static String createConnectionString(String hostName, String username, String password, String keyPath, String passphrase,
            String remoteFilePath) {

        if (keyPath != null) {
            return "sftp://" + username + "@" + hostName + "/" + remoteFilePath;
        } else {
            return "sftp://" + username + ":" + password + "@" + hostName + "/" + remoteFilePath;
        }
    }


    public static void main(String[] args) throws IOException, URISyntaxException, JSchException, SftpException {
        startUploadJSCH();
    }

    private static ChannelSftp setupJsch() throws JSchException {
        JSch jsch = new JSch();
        String privateKey = "~/.ssh/backup_key_minh/id_rsa";  
        
        java.util.Properties config = new java.util.Properties(); 
        config.put("StrictHostKeyChecking", "no");
        
        jsch.addIdentity(privateKey);
        com.jcraft.jsch.Session jschSession = jsch.getSession("hungnt", "118.68.168.59", 22);
        jschSession.setConfig(config);
        jschSession.connect();

        return (ChannelSftp) jschSession.openChannel("sftp");
    }

    public static void startUploadJSCH() throws JSchException, SftpException, MalformedURLException, IOException {

        FileUtils.copyURLToFile(
                new URL("https://gcdn.2mdn.net/videoplayback/id/61cd719725d3fc3b/itag/18/source/doubleclick_dmm/ctier/L/acao/yes/ip/0.0.0.0/ipbits/0/expire/3742967498/sparams/id,itag,source,ctier,acao,ip,ipbits,expire/signature/AD0E889732223C81AC2AB8FDC712B4A5DB19669D.716DAD658EBE6B7A7E545A8A51EF58196F62BB75/key/ck2/file/file.mp4"),
                new File("/home/hungnt/tnhminh/AdsProject/Adsplay_Ver2/FTEL-BACKEND/cdn-delivery-1.0/resources/cdn-local/minh.mp4"));

        ChannelSftp channelSftp = setupJsch();
        channelSftp.connect();

        String localFile = "/home/hungnt/tnhminh/AdsProject/Adsplay_Ver2/FTEL-BACKEND/cdn-delivery-1.0/resources/cdn-local/minh.mp4";
        String remoteDir = ROOT_DIRECTORY;

        channelSftp.put(localFile, remoteDir + "minhnew.mp4");

        channelSftp.exit();
    }
}
