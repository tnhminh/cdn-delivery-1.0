package adsplay.server.delivery.handler;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.ua.Client;
import rfx.core.util.LogUtil;

public class VideoTvcAdHandler {

  public static void process(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders,
      MultiMap params, Client userAgentClient, int platformId, int placementId, String useragent,
      String refererHeaderUrl, String origin) throws Exception {
	  
  }

  public static void servingMobileVast3(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders, MultiMap params, Client userAgentClient, int platformId, int placementId,
      String useragent) throws Exception {
	  
  }


  // --------------------------------------------------------------------------------

  public static String getChannelName(String url) {
    int idx = url.indexOf("/livetv");
    boolean isLiveTV = idx > 0;
    if (isLiveTV) {
      String channelName = url.endsWith("/livetv") ? "vtv3" : url.substring(idx + 8);

      int idx1 = channelName.indexOf("#");
      if (idx1 > 0) {
        channelName = channelName.substring(0, idx1);
      }

      int idx2 = channelName.indexOf("?");
      if (idx2 > 0) {
        channelName = channelName.substring(0, idx2);
      }

      int idx3 = channelName.indexOf("/");
      if (idx3 > 0) {
        channelName = channelName.substring(0, idx3);
      }

      return channelName;
    }
    return "";
  }

    public static String getContentIdFromURL(String url) {
        String id = null;

        if (url.contains("livetv")) {
            int beginIndex = url.lastIndexOf("/") + 1;
            try {
                id = url.substring(beginIndex);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (url.contains("xem-truyen-hinh")) {
            try {
                return url.substring(url.indexOf("xem-truyen-hinh/") + 16);
            } catch (Exception e) {
                LogUtil.error(VideoTvcAdHandler.class, "getContentIdFromURL",
                        "Failure when getContentIdFromURL from url for xem-truyen-hinh: " + url + ", finalId : " + id);
            }
        } else {
            try {
                int idx1 = url.lastIndexOf("/") + 1;
                int idx2 = url.indexOf("-");
                if (idx1 > 1 && idx2 > idx1) {
                    id = url.substring(idx1, idx2);
                } else if (idx1 > 1) {
                    id = url.substring(idx1);
                }

                // If id still be the episode of film
                if (url.contains("vod-ads/")) {
                    id = url.substring(url.indexOf("vod-ads/") + 8, url.lastIndexOf("/"));
                } else if (id.matches("\\d+")) {
                    id = url.substring(url.indexOf("vod/") + 4, url.lastIndexOf("/"));
                    LogUtil.i("Check id is episode of film", "Final contentID:" + id, true);
                } else if (id.endsWith(".html")) {
                    id = id.substring(id.lastIndexOf("-") + 1, id.indexOf(".html"));
                } else if (id.contains("tap-")){
                    id = id.replaceAll("#tap-.+", "");
                    id = id.substring(id.lastIndexOf("-") + 1, id.indexOf(".html"));
                } else {
                    // Get final string in case of default
                    id = url.substring(url.lastIndexOf("/") + 1);
                }

            } catch (Throwable e) {
                LogUtil.error(VideoTvcAdHandler.class, "getContentIdFromURL", "Failure when getContentIdFromURL from url: " + url + ", finalId : " + id);
            }
        }

        return id;
    }


  public static void updatePlayViewRealtimeStats(int platformId, int placementId) {
	  
  }

  public static void updatePlayViewRealtimeStats(HttpServerRequest req, Client client,
      int platformId, int placement, String uuid) {
	  
  }

}
