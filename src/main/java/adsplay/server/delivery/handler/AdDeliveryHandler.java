package adsplay.server.delivery.handler;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

public class AdDeliveryHandler {

  public static final String ORIGIN = "Origin";
  public static final String USER_AGENT = "User-Agent";
  public static final String REFERER = "Referer";
  public static final String DISPLAY = "display";
  public static final String OVERLAY = "overlay";
  public static final String REACTIVE = "reactive";
  public static final String NATIVE = "native";


  private static final String TVC = "tvc";
  public static final String NO_DATA = "0";
  public static final String HTTP_FPTPLAY_NET = "http://fptplay.net";
  public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
  public static final String APPLICATION_JSON = "application/json";
  public static final String APPLICATION_XML = "application/xml";

  public static void adHandlerVersion1(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
	  
  }

  public static void adHandlerMobile(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
	  
  }

  public static void adxVideoHandler(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
	  
  }

  public static void adLoad(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders)
      throws Exception {
	  
  }



}
