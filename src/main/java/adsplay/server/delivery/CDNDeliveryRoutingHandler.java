package adsplay.server.delivery;

import static io.vertx.core.http.HttpHeaders.CONNECTION;
import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpStatus;

import com.adsplay.cdn.trancoded.CdnHandler;

import adsplay.common.BaseHttpLogHandler;
import adsplay.common.util.ContainerUtils;
import adsplay.delivery.properties.DeliveryConfiguration;
import adsplay.delivery.properties.MessageProperties;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.net.SocketAddress;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;
import server.http.util.HttpTrackingUtil;

public class CDNDeliveryRoutingHandler extends BaseHttpLogHandler {

    public static final String CDN_DELIVERY = "/thirdpt/cdn/get";

    private static final int MAX_TIMED_OUT_PER_REQUEST_FPLAY = 950;

    public static final String ADSPLAY_NET = "adsplay.net";
    public static final String APLUUID = "apluuid";

    public static final String POWERED_BY = MessageProperties.getProperty("general.powered.by", "");
    public static final String ADS_PLAY_VERSION = MessageProperties.getProperty("general.ads.play.version", "");
    public static final String CONTAINER_POD_NAME = "Container-Pod-Name";

    public static final String HTTP = "http://";
    private static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();
    public static final String BASE_DOMAIN = WORKER_CONFIGS.getCustomConfig("baseServerDomain");
    public static final String LOG1_DOMAIN = WORKER_CONFIGS.getCustomConfig("log1ServerDomain");
    public static final String LOG2_DOMAIN = WORKER_CONFIGS.getCustomConfig("log2ServerDomain");
    public static final String LOG3_DOMAIN = WORKER_CONFIGS.getCustomConfig("log3ServerDomain");
    public static final String LOG4_DOMAIN = WORKER_CONFIGS.getCustomConfig("log4ServerDomain");

    public CDNDeliveryRoutingHandler() {
    }

    @Override
    public void handle(HttpServerRequest request) {
        // Routing
    }

    static final String unknown = "unknown";

    private static final List<String> RM_IPS = new ArrayList<String>();
    static {
        RM_IPS.add("X-Real-IP");
    }

    public static String getRemoteIP(HttpServerRequest request) {
        String ipAddress = StringUtil
                .safeString(request.headers().get("X-Forwarded-For"), StringUtil.safeString(request.headers().get("X-FORWARDED-FOR")))
                .trim();
        StringBuilder logIp = new StringBuilder();

        RM_IPS.forEach(rmIp -> {
            logIp.append("key:" + rmIp + "-").append(StringUtil.safeString(request.headers().get(rmIp))).append("|");
        });

        if (!StringUtil.isNullOrEmpty(ipAddress) && !unknown.equalsIgnoreCase(ipAddress)) {
            // LogUtil.dumpToFileIpLog(ipAddress);
            String[] toks = ipAddress.split(",");
            int len = toks.length;
            if (len > 1) {
                ipAddress = toks[len - 1];
            } else {
                return ipAddress;
            }
        } else {
            ipAddress = getIpAsString(request.remoteAddress());
        }
        return ipAddress;
    }

    public static String getIpAsString(SocketAddress address) {
        try {
            if (address instanceof InetSocketAddress) {
                return ((InetSocketAddress) address).getAddress().getHostAddress();
            }
            return address.toString().split("/")[1].split(":")[0];
        } catch (Throwable e) {
            LogUtil.error(CDNDeliveryRoutingHandler.class, "getIpAsString", "Failure when getIpAsString with address:" + address.toString());
        }
        return "0.0.0.0";
    }

    private static ExecutorService blockingJobService = Executors
            .newFixedThreadPool(Integer.valueOf(DeliveryConfiguration.getProperty("job.worker.pool.size", "100")));

    private static int timedOutFplay = MAX_TIMED_OUT_PER_REQUEST_FPLAY;

    public static void runWorker(String uri, MultiMap params, Future<String> futureExeBlock, HttpServerRequest request,
            Map<String, Object> additionalInfos) {

        // Do the blocking operation in here
        String rsText = "";

        int workerTimedOut = timedOutFplay;

        java.util.concurrent.Future<String> futureTask = blockingJobService.submit(new Callable<String>() {

            @Override
            public String call() throws Exception {
                return execute(uri, params, additionalInfos);
            }
        });
        try {
            rsText = futureTask.get(workerTimedOut, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof TimeoutException) {
                LogUtil.error(CDNDeliveryRoutingHandler.class, "runWorker", "Timed out worker thread with uri:" + uri);
                rsText = "{\"message\":\"Timed out request\", \"status\":\"408 Request Timeout\"}";
            } else {
                LogUtil.error(CDNDeliveryRoutingHandler.class, "runWorker",
                        "InterruptedException/ExecutionException when execute delivery task, uri:" + uri);
                rsText = "{\"message\":\"" + e.getMessage() + "\", \"status\":\"500 Internal Server Error\"}";
            }
            LogUtil.error(e);
        } finally {
            futureTask.cancel(true);
            futureTask = null;
        }

        futureExeBlock.complete(rsText);
    }

    private static String execute(String uri, MultiMap params, Map<String, Object> additionalInfos) {
        return CdnHandler.returnCdnUrl(uri, params);
    }

    public static void routingToHandler(HttpServerRequest request, Vertx vertxInstance) {

        Map<String, Object> additionalInfos = new HashMap<String, Object>();

        try {
            String uri = StringUtil.safeString(request.uri(), "");

            if (uri.contains("favicon.ico")) {
                String responseStatus = "{\"message\":\"422 Unprocessable Entity\", \"status\":\"Placement param not found on this request\",  \"uri\":\""
                        + uri + "\"}";
                request.response().end(responseStatus);
                return;
            }

            // Adding executeBlocking here
            vertxInstance.<String>executeBlocking(future -> {
                // Do work main
                runWorker(uri, request.params(), future, request, additionalInfos);
            }, res -> {
                try {
                    String origin = StringUtil.safeString(request.headers().get("orign"), "");
                    MultiMap params = request.params();
                    if (res.succeeded()) {
                        HttpServerResponse response = request.response();
                        doResponse(uri, res.result(), new ArrayList<String>(), response, params, origin, additionalInfos);
                        LogUtil.i(CDNDeliveryRoutingHandler.class, "Response result to client: " + uri + "\n===========END===========", true);
                    } else {

                        LogUtil.error(CDNDeliveryRoutingHandler.class, "routingToHandler", "Failure when doing response to client !!!");
                        LogUtil.error(res.cause());
                        HttpServerResponse response = request.response();
                        doResponse(uri, "", new ArrayList<String>(), response, params, origin, additionalInfos);
                    }
                } catch (Exception e) {
                    String err = e.getMessage();
                    request.response().end(err);
                } finally {
                    // Refresh mem
                    additionalInfos.clear();
                }
                return;
            });

        } catch (Throwable e) {
            String err = e.getMessage();
            request.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
        }
    }

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerResponse response, MultiMap params,
            String origin, Map<String, Object> additionalInfos) {
        MultiMap outHeaders = response.headers();
        outHeaders.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);
        outHeaders.set(POWERED_BY, ADS_PLAY_VERSION);
        outHeaders.set(CONTAINER_POD_NAME, ContainerUtils.getName());
        outHeaders.set(HttpHeaders.createOptimized("x-response-time"),
                System.currentTimeMillis() - StringUtil.safeParseLong(params.get("cb"), 0) + "");

        // Keep here (don't remove)
        setCorsHeaders(outHeaders, pmIds, origin);

        if (rsText.contains("408 Request Timeout")) {
            outHeaders.set("MessageCause", rsText);
            response.setStatusCode(408);
            response.end();
            return;
        } else if (rsText.contains("500 Internal Server Error")) {
            outHeaders.set("MessageCause", rsText);
            response.setStatusCode(500);
            response.end();
            return;
        } else {
            if (!rsText.isEmpty()) {
                outHeaders.set(HttpHeaders.CONTENT_TYPE, "text/html");
                response.end(rsText);
                return;
            }
        }
    }

    public static final String GIF = "image/gif";
    public static final String HEADER_CONNECTION_CLOSE = "Close";
    public static final String BASE64_GIF_BLANK = "R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerRequest req) {

        Buffer buffer = Buffer.buffer(BASE64_GIF_BLANK);
        HttpServerResponse response = req.response();
        MultiMap headers = response.headers();
        headers.set(CONTENT_TYPE, GIF);
        headers.set(CONTENT_LENGTH, String.valueOf(buffer.length()));
        headers.set(CONNECTION, HEADER_CONNECTION_CLOSE);
        setCorsHeaders(headers);
        response.end(buffer);

    }

    private final static void setCorsHeaders(MultiMap headers) {
        headers.set("Access-Control-Allow-Origin", "*");
        headers.set("Access-Control-Allow-Credentials", "true");
        headers.set("Access-Control-Allow-Methods", "POST, GET");
        headers.set("Access-Control-Allow-Headers", "origin, content-type, accept, Set-Cookie");
    }
}
