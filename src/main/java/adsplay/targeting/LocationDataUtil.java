package adsplay.targeting;

import java.util.HashMap;
import java.util.Map;

public class LocationDataUtil {
	public static final String VN_SOUTH = "vn-south";
	public static final String VN_NORTH = "vn-north";
	public static final Map<String,Boolean> southCities = new HashMap<>(15);
	public static final Map<String,Boolean> northCities = new HashMap<>(15);
	static {
		southCities.put("THANH PHO HO CHI MINH", true);
		southCities.put("BIEN HOA", true);
		southCities.put("CAN THO", true);
		southCities.put("THU DAU MOT", true);
		southCities.put("VUNG TAU", true);
		southCities.put("CA MAU", true);
		southCities.put("CAN THO", true);
		southCities.put("NHA TRANG", true);
		southCities.put("LONG XUYEN", true);
		southCities.put("DA LAT", true);
		southCities.put("BAC LIEU", true);
		southCities.put("CAN GIO", true);
		southCities.put("TAY NINH", true);		
		
		northCities.put("HA NOI", true);
		northCities.put("VINH", true);
		northCities.put("HA DONG", true);
		northCities.put("HUE", true);
		northCities.put("HAIPHONG", true);
		northCities.put("UONG BI", true);
		northCities.put("BAC NINH", true);
		northCities.put("HOA BINH", true);
		northCities.put("SON TAY", true);
		northCities.put("LANG SON", true);
		northCities.put("TURAN", true);
		northCities.put("THAI BINH", true);
		northCities.put("THANH HOA", true);
	}
}
