package adsplay.delivery.properties;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Properties;

import rfx.core.util.FileUtils;

public class MessageProperties {

    private static final String CONFIG_PATH = "/" + "configs/" + "message.properties";

    private static Properties properties = new Properties();

    private MessageProperties() {
    }

    static {
        init();
    }

    private static void init() {
        DataInputStream streamConfigFile = FileUtils.readFileAsStream(CONFIG_PATH);
        try {
            properties.load(streamConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return getProperty(key, "");
    }

    public static String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public static void main(String[] args) {
        System.out.println(properties.getProperty("placement.ad_view.field"));
    }
}
