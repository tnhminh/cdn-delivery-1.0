package adsplay.delivery.redis.cache;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class RedisDataCache {

    private static RedisDataCache cache = new RedisDataCache();

    public static LoadingCache<String, Map<String, String>> data = CacheBuilder.newBuilder().maximumSize(500).expireAfterWrite(3, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Map<String, String>>() {
                public Map<String, String> load(String key) {
                    return null;
                }
            });

    private RedisDataCache() {

    }

    public static RedisDataCache getInstance() {
        return cache;
    }

}
