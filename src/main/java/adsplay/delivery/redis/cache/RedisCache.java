package adsplay.delivery.redis.cache;

import java.util.HashMap;
import java.util.Map;

import adsplay.delivery.redis.cache.filter.RedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.LogUtil;

public class RedisCache {

    private static final String CDN_CACHE = "cdn-cache";

    static ShardedJedisPool cdnCache = RedisConfigs.load().get(CDN_CACHE).getShardedJedisPool();

    public static void init() {
    }

    public static void incrBy(final String key, int expiredTime, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;
            if (redisPool.equals(RedisPool.CDN_CACHE)) {
                redisPoolProxy = cdnCache;
            }

            new RedisCommand<Void>(redisPoolProxy) {
                @Override
                protected Void build() {
                    try {
                        Long oldKey = jedis.ttl(key);
                        Pipeline p = jedis.pipelined();
                        p.incr(key);
                        if (oldKey < 0) {
                            p.expire(key, expiredTime);
                        }
                        p.sync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return null;
                }
            }.execute();
        } catch (Exception e) {
            LogUtil.error(e);
        }
    }

    public static Map<String, String> hget(final String key, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;
            if (redisPool.equals(RedisPool.CDN_CACHE)) {
                redisPoolProxy = cdnCache;
            }
            Map<String, String> result = new RedisCommand<Map<String, String>>(redisPoolProxy) {
                @Override
                protected Map<String, String> build() {
                    try {
                        return jedis.hgetAll(key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return new HashMap<>();
                }
            }.execute();
            return result;
        } catch (Exception e) {
            LogUtil.error(e);
        }
        return new HashMap<>();
    }

    public static String hget(final String key, final String fieldName, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;

            if (redisPool.equals(RedisPool.CDN_CACHE)) {
                redisPoolProxy = cdnCache;
            }
            String result = new RedisCommand<String>(redisPoolProxy) {
                @Override
                protected String build() {
                    try {
                        return jedis.hget(key, fieldName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return "";
                }
            }.execute();
            return result;
        } catch (Exception e) {
            LogUtil.error(e);
        }
        return "";
    }

    public static String get(final String key, RedisPool poolName) {
        String result = "";
        ShardedJedisPool redisPoolProxy = null;
        try {
            if (poolName.equals(RedisPool.CDN_CACHE)) {
                redisPoolProxy = cdnCache;
            }

            result = new RedisCommand<String>(redisPoolProxy) {
                @Override
                protected String build() {
                    try {
                        return jedis.get(key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return "";
                }
            }.execute();
            return result;
        } catch (Exception e) {
            LogUtil.error(RedisCache.class, "get", "Error when redis get key:" + key + ", poolName:" + poolName);
            LogUtil.error(e);
        }
        return "";

    }

    public static void hset(final String key, final String field, final String value, RedisPool poolName) {
        ShardedJedisPool redisPoolProxy = null;
        try {
            if (poolName.equals(RedisPool.CDN_CACHE)) {
                redisPoolProxy = cdnCache;
            }
            new RedisCommand<Void>(redisPoolProxy) {
                @Override
                protected Void build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        p.hset(key, field, value);
                        p.sync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return null;
                }
            }.execute();
        } catch (Exception e) {
            LogUtil.error(e);
        }
    }

    public static boolean insertDummyData() {
        boolean commited = false;
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(cdnCache) {
                @Override
                protected Boolean build() throws JedisException {
                    Pipeline p = jedis.pipelined();
                    Map<String, String> startTimeMap = new HashMap<String, String>();
                    startTimeMap.put("type", "mid_roll");
                    startTimeMap.put("value", "00:00:10");

                    Map<String, String> creativeMap = new HashMap<String, String>();
                    creativeMap.put("source", "http://cdn.adsplay.net/2018/01/video.mp4");
                    creativeMap.put("landing_page", "https://www.vinamilk.com.vn");
                    creativeMap.put("skip_time", "00:00:05");
                    creativeMap.put("duration", "00:00:30");
                    creativeMap.put("flightID", "20");
                    creativeMap.put("start_time", startTimeMap.toString());
                    p.hmset("creative_12", creativeMap);

                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commited;
    }

    public static void main(String[] args) {

    }
}
