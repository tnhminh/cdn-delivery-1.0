package com.adsplay.cdn.trancoded;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import adsplay.common.util.UploadUtils;
import rfx.core.util.LogUtil;

public class CdnJobWorker {

    private static final int MAX_JOB_QUEUE = 1;

    private static Timer m_timer = new Timer();

    private static LinkedBlockingQueue<CdnTrancodedJob> m_requestQueues = new LinkedBlockingQueue<CdnTrancodedJob>();

    static {
        initJob();
    }

    public static void initJob() {
        m_timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                try {
                    runJobQueue();
                } catch (Exception e) {
                    LogUtil.error(CdnJobWorker.class, "initJob", "Error when runJobQueue.");
                    LogUtil.error(e);
                }
            }

        }, 500, 2000);
    }

    private static void runJobQueue() {
        
        LinkedBlockingQueue<CdnTrancodedJob> maxExecuteQueues = new LinkedBlockingQueue<CdnTrancodedJob>(MAX_JOB_QUEUE);

        if (m_requestQueues.isEmpty()) {
            return;
        }

        int i = 0;
        while (i < MAX_JOB_QUEUE && !m_requestQueues.isEmpty()) {
            try {
                maxExecuteQueues.offer(m_requestQueues.poll(5000, TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                LogUtil.error(CdnJobWorker.class, "runJobQueue", "Error when polling request.");
                LogUtil.error(e);
            }
            i++;
        }

        if (!maxExecuteQueues.isEmpty()) {
            // do main job with list maxExecuteQueues
            maxExecuteQueues.stream().forEach(job -> {
                try {
                    UploadUtils.startUploadVFS(job);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            });
            LogUtil.i(CdnJobWorker.class, "Update request to redis, size:" + maxExecuteQueues.size(), true);
            maxExecuteQueues.clear();
        }
    }

    public static void executeRequestJob(CdnTrancodedJob job) {
        boolean isSuccess = false;
        if (job != null) {
            isSuccess = m_requestQueues.offer(job);
            if (!isSuccess) {
                LogUtil.i(CdnJobWorker.class, "Adding request job fail !!!", true);
            }
        }
    }

}
