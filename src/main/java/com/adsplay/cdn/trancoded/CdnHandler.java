package com.adsplay.cdn.trancoded;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.GsonBuilder;

import adsplay.common.util.PlacementConverter;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.server.delivery.CDNDeliveryRoutingHandler;
import io.vertx.core.MultiMap;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class CdnHandler {

    public static String returnCdnUrl(String uri, MultiMap params) {
        String trancodedCdn = "";
        Map<String, Object> paramsAsMap = new HashMap<String, Object>();
        if (uri.startsWith(CDNDeliveryRoutingHandler.CDN_DELIVERY)) {
            paramsAsMap.put("uri", uri);
            // URI Pattern: thirdpt/cdn/get?uriCDN=&pid=

            String current3rdPtCDN = StringUtil.safeString(params.get("uriCDN"));
            String placementID = StringUtil.safeString(params.get("pid"));

            paramsAsMap.put("current3rdPtCDN", current3rdPtCDN);
            paramsAsMap.put("placementID", placementID);

            // Get trancoded video from redis mapping
            trancodedCdn = StringUtil.safeString(RedisCache.hget("cdn_trancoded_mapping", placementID + "-" + current3rdPtCDN, RedisPool.CDN_CACHE), "");
            paramsAsMap.put("trancodedCdn", trancodedCdn);

            // Check video was trancoded successfully at System Side?
            String isTrancodedComplete = StringUtil.safeString(RedisCache.hget("cdn_trancoded_completed", trancodedCdn, RedisPool.CDN_CACHE), "");
            paramsAsMap.put("isTrancoded", isTrancodedComplete);

            boolean trancodedEmpty = trancodedCdn.isEmpty();
            paramsAsMap.put("trancodedCdn.isEmpty()", trancodedEmpty);

            if (trancodedEmpty) {
                // If mapping return empty, response current 3rd pt cdn, and
                // then adding the job to queue
                CdnJobWorker.executeRequestJob(new CdnTrancodedJob(placementID, current3rdPtCDN, PlacementConverter.convert(placementID), paramsAsMap));
                trancodedCdn = current3rdPtCDN;
            } else if (!"1".equals(isTrancodedComplete)) {
                //trancodedCdn = current3rdPtCDN;
            }
        }

        LogUtil.i(CdnHandler.class, "[Detailed-CDN-Log]-" + new Date() + "-" + new GsonBuilder().create().toJson(paramsAsMap), true);
        paramsAsMap.clear();
        paramsAsMap = null;

        return trancodedCdn;
    }
}
