package com.adsplay.cdn.trancoded;

import java.util.HashMap;
import java.util.Map;

public class CdnTrancodedJob {

    private String placementId = "";
    private String urlCdn = "";

    private String remoteDir = "";

    private Map<String, Object> paramsAsmap = new HashMap<String, Object>();

    public CdnTrancodedJob(String urlCdn, String remoteDir, Map<String, Object> paramsAsmap) {
        super();
        this.urlCdn = urlCdn;
        this.remoteDir = remoteDir;
        this.paramsAsmap = paramsAsmap;
    }

    public CdnTrancodedJob(String placementId, String urlCdn, String remoteDir, Map<String, Object> paramsAsmap) {
        super();
        this.placementId = placementId;
        this.urlCdn = urlCdn;
        this.remoteDir = remoteDir;
        this.paramsAsmap = paramsAsmap;
    }

    public String getPlacementId() {
        return placementId;
    }

    public void setPlacementId(String placementId) {
        this.placementId = placementId;
    }

    public Map<String, Object> getParamsAsmap() {
        return paramsAsmap;
    }

    public void setParamsAsmap(Map<String, Object> paramsAsmap) {
        this.paramsAsmap = paramsAsmap;
    }

    public String getUrlCdn() {
        return urlCdn;
    }

    public void setUrlCdn(String urlCdn) {
        this.urlCdn = urlCdn;
    }

    public String getRemoteDir() {
        return remoteDir;
    }

    public void setRemoteDir(String remoteDir) {
        this.remoteDir = remoteDir;
    }

}
