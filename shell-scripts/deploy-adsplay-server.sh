#!/bin/sh

CODE="BUILD-OUTPUT/adsplay-server-1.0.jar"
REMOTE_FOLDER="/build/adsplay-server"

# Server 50
SERVER50="thaint@118.69.184.50"
scp -P 228 -r $CODE $SERVER50:$REMOTE_FOLDER
echo " ===> Deployed $SERVER50 at $(date)"
ssh -p 228 $SERVER50 'sudo bash -s' <  shell-scripts/restart-server-50.sh
echo " Restarted Ok $SERVER50 at $(date) \n"

# Server 46
SERVER46="thaint@118.69.190.46"
scp -P 228 -r $CODE $SERVER46:$REMOTE_FOLDER
echo " ===> Deployed $SERVER46 at $(date)"
ssh -p 228 $SERVER46 'sudo bash -s' <  shell-scripts/restart-server-46.sh 
echo " Restarted Ok $SERVER46 at $(date) \n"

# Server 49
SERVER49="thaint@118.69.184.49"
scp -P 228 -r $CODE $SERVER49:$REMOTE_FOLDER
echo " ===> Deployed $SERVER49 at $(date)"
ssh -p 228 $SERVER49 'sudo bash -s' <  shell-scripts/restart-server-49.sh 
echo " Restarted Ok $SERVER49 at $(date) \n"

# Server 87
SERVER87="thaint@42.119.252.87"
scp -P 228 -r $CODE $SERVER87:$REMOTE_FOLDER
echo " ===> Deployed $SERVER87 at $(date)"
ssh -p 228 $SERVER87 'sudo bash -s' <  shell-scripts/restart-server-87.sh
echo " Restarted Ok $SERVER87 at $(date) \n"

# Server 88
SERVER88="thaint@42.119.252.88"
scp -P 228 -r $CODE $SERVER88:$REMOTE_FOLDER
echo " ===> Deployed $SERVER88 at $(date)"
ssh -p 228 $SERVER88 'sudo bash -s' <  shell-scripts/restart-server-88.sh
echo " Restarted Ok $SERVER88 at $(date) \n"

# Server 41
SERVER41="thaint@118.69.190.41"
scp -P 228 -r $CODE $SERVER41:$REMOTE_FOLDER
echo " ===> Deployed $SERVER41 at $(date)"
ssh -p 228 $SERVER41 'sudo bash -s' <  shell-scripts/restart-server-41.sh
echo " Restarted Ok $SERVER41 at $(date) \n"

# SERVER46="thaint@118.69.190.46"
# scp -P 228 -r $CODE $SERVER46:$REMOTE_FOLDER
# echo " ===> Done $SERVER46 at $(date)"

echo "---- DONE ---"