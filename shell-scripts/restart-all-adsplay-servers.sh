#!/bin/sh

# Server 50
SERVER50="trieunt@118.69.184.50"
ssh -p 228 $SERVER50 'sudo bash -s' <  shell-scripts/restart-server-50.sh
echo " Restarted Ok $SERVER50 at $(date) \n"

# Server 49
SERVER49="trieunt@118.69.184.49"
ssh -p 228 $SERVER49 'sudo bash -s' <  shell-scripts/restart-server-49.sh 
echo " Restarted Ok $SERVER49 at $(date) \n"

# Server 46
SERVER46="trieunt@118.69.190.46"
ssh -p 228 $SERVER46 'sudo bash -s' <  shell-scripts/restart-server-46.sh 
echo " Restarted Ok $SERVER46 at $(date) \n"

# Server 87
SERVER87="trieunt@42.119.252.87"
ssh -p 228 $SERVER87 'sudo bash -s' <  shell-scripts/restart-server-87.sh
echo " Restarted Ok $SERVER87 at $(date) \n"

# Server 88
SERVER88="trieunt@42.119.252.88"
ssh -p 228 $SERVER88 'sudo bash -s' <  shell-scripts/restart-server-88.sh
echo " Restarted Ok $SERVER88 at $(date) \n"

# Server 41
SERVER41="trieunt@118.69.190.41"
ssh -p 228 $SERVER41 'sudo bash -s' <  shell-scripts/restart-server-41.sh
echo " Restarted Ok $SERVER41 at $(date) \n"

echo "---- DONE 5 servers ---"