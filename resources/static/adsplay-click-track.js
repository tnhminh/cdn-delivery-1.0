
if( ! window.AdsPlayClickTrackReady ){	
	(function(global, undefined) {
		var AdsPlayClickTrack = {};
		
		function callBeaconLogTracking(opts) {
			// Make sure we have a base object for opts
			opts = opts || {};
			// Setup defaults for options
			opts.url = opts.url || null;
			opts.vars = opts.vars || {};
			opts.error = opts.error || function() {
			};
			opts.success = opts.success || function() {
			};
			opts.vars.cb = Math.floor(Math.random() * 10e12);

			// Split up vars object into an array
			var varsArray = [];
			for ( var key in opts.vars) {
				varsArray.push(encodeURIComponent(key) + '='
						+ encodeURIComponent(opts.vars[key]));
			}
			// Build query string
			var qString = varsArray.join('&');

			// Create a beacon if a url is provided
			if (opts.url) {
				// Create a brand NEW image object
				var beacon = new Image();
				// Attach the event handlers to the image object
				if (beacon.onerror) {
					beacon.onerror = opts.error;
				}
				if (beacon.onload) {
					beacon.onload = opts.success;
				}

				// Attach the src for the script call
				beacon.src = opts.url + '?' + qString;
			}
		}
		var baseUrlTrackingAds = "https://log.adsplay.net/track/ads";

		function getHashParams() {
			var hashParams = {};
			var e, a = /\+/g, // Regex for replacing addition symbol with a space
			r = /([^&;=]+)=?([^&;]*)/g, d = function(s) {
				return decodeURIComponent(s.replace(a, " "));
			}, q = window.location.hash.substring(1);
			while (e = r.exec(q))
				hashParams[d(e[1])] = d(e[2]);
			return hashParams;
		}

		function handleClickAndTracking() {
			try {
				var fullParams = getHashParams();
				//console.log(fullParams);
				var clicktag, adid, beacon;

				adid = fullParams['adid'];
				beacon = fullParams['beacon'];
				clicktag = fullParams['clicktag'];

				//window.open(clicktag, "_blank");
				
				document.getElementById('ads-default').setAttribute('href', clicktag);
				
				//callBeaconLogTracking({	url : baseUrlTrackingAds,vars : {metric : 'click',adid : adid,beacon : beacon }});
			} catch (e) {
				console.log(e);
			}
		}
		AdsPlayClickTrack.handleClickAndTracking = handleClickAndTracking;
		
		
		global.AdsPlayClickTrack = AdsPlayClickTrack;
	})(typeof window === 'undefined' ? this : window);	
	window.AdsPlayClickTrackReady = true;
}

AdsPlayClickTrack.handleClickAndTracking();