
<div id='adsplay-container'>
  <script type="text/javascript" src="https://ads-cdn.fptplay.net/static/sdk/website/sdk/crfn.js?v=0702"></script>
  <script type='text/javascript'>
    function noData() {
      htmlLogTracking('nodata');
      setTimeout(function() {
        emit('closeBanner');
      }, 1000);
    }
    
    function deliveryReplaceTracking() {
      api_request = [api_request]; 
      api_nodata = [api_nodata]; 
      setupFollowTracking();
    }

    function init(width, height) {
      deliveryReplaceTracking();

      setTimeout(noData, 1);
    }

    function loading() {
      if (typeof crfnLoaded !== 'undefined') {
        init();
      } else {
        setTimeout(loading, 500);
      }
    }

    loading();

  </script>
</div>