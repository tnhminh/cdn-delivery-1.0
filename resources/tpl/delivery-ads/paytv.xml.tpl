<?xml version="1.0" encoding="UTF-8"?>
<VAST xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.0" xsi:noNamespaceSchemaLocation="vast.xsd">
    <Ad id="0">
        <InLine>
            <AdSystem version="2.0">iTVAds</AdSystem>
            <AdTitle>In-Stream PayTV Video</AdTitle>
            <Impression id="">
                <![CDATA[ ]]>
            </Impression>
            <Creatives>
                <Creative sequence="1" AdID="{{adID}}">
                    <Linear>
                        <Duration>{{duration}}</Duration>
                        <AdParameters/>
                        <TrackingEvents>
                            <Tracking event="midpoint">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="firstQuartile">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="thirdQuartile">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="complete">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="mute">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="unmute">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="pause">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="rewind">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="resume">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="fullscreen">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="collapse">
                                <![CDATA[ ]]>
                            </Tracking>
                            <Tracking event="close">
                                <![CDATA[ ]]>
                            </Tracking>
                        </TrackingEvents>
                        <VideoClicks>
                            <ClickThrough>
                                <![CDATA[]]>
                            </ClickThrough>
                            <ClickTracking id="iTVAds">
                                <![CDATA[ ]]>
                            </ClickTracking>
                        </VideoClicks>
                        <MediaFiles>
                            <MediaFile id="1" delivery="progressive" type="video/mp4" bitrate="800" width="640" height="360" maintainAspectRatio="true" scalable="true">
                                <![CDATA[{{adFile}}]]>
                            </MediaFile>
                        </MediaFiles>
                    </Linear>
                </Creative>
            </Creatives>
        </InLine>
    </Ad>
</VAST>