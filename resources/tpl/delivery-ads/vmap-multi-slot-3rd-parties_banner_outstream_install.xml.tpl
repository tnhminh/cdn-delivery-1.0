<?xml version="1.0" encoding="UTF-8"?>
<vmap:VMAP xmlns:vmap="http://www.iab.net/videosuite/vmap" version="1.0">
	{{#each adBreaks }}
	<vmap:AdBreak timeOffset="{{timeOffset}}" breakType="{{breakType}}" breakId="{{breakId}}">
	
		{{#each instreamVideoAds}}
		<vmap:AdSource id="{{adId}}" allowMultipleAds="false" followRedirects="true">
		<vmap:VASTAdData>
			<VAST version="{{vastVersion}}">
			   <Ad id="{{adId}}">
			   
			   {{#if adTagURI}}
			      <Wrapper fallbackOnNoAd="true">
			   {{/if}}
			   {{#unless adTagURI}}
			      <InLine>
			   {{/unless}}
			         <AdSystem version="1.0">AdsPLAY</AdSystem>
			         <AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
			         <Description>
							<![CDATA[{{description}}]]>
					 </Description>
			         {{#if adTagURI}}
			         	<VASTAdTagURI><![CDATA[{{adTagURI}}]]></VASTAdTagURI>
			         {{/if}}
			         <Error>
						<![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=error&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}&errorCode=[ERRORCODE]]]>
					 </Error>
			         
			         <Error>
						<![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]>
					 </Error>
					 
			         <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=impression&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Impression>
			         
			         <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Impression>

			         {{#each impression3rdUrls}}
			         <Impression><![CDATA[{{this}}]]></Impression>
			         {{/each}}
			         <Creatives>
			            <Creative sequence="1" id="{{adId}}">
			            
			            {{#if adTagURI}}
			            	{{#if skipoffset}}
			               <Linear skipoffset="{{skipoffset}}">
			            	{{/if}}
			            	{{#unless skipoffset}}
			            	<Linear>
			            	{{/unless}}
   			            {{/if}}

			            {{#unless adTagURI}}
			               <Linear skipoffset="{{skipoffset}}">
			                  <Duration>{{duration}}</Duration>
			            {{/unless}}      
			                  <TrackingEvents>
			                     {{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=creativeView&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played0&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played25&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played50&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played75&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played100&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=close&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                  	 <Tracking event="pause"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=paused&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                  	 <Tracking event="mute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=mute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                  	 <Tracking event="unmute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=unmute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
								 <Tracking event="skip"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=skip&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]]]></Tracking>
			                  </TrackingEvents>
			                  
				              {{#if adTagURI}}
				                  <VideoClicks>
				                  {{videoClicksTags}}
				                  <ClickTracking><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=click&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&isFallbackAd={{isFallbackAd}}&imaCB=[TIMESTAMP]]]></ClickTracking>
				                  </VideoClicks>
			                  {{/if}}
			                  
				              {{#unless adTagURI}}
				                  <VideoClicks>
				                     {{videoClicksTags}}
				                     <ClickTracking><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=click&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&isFallbackAd={{isFallbackAd}}&imaCB=[TIMESTAMP]]]></ClickTracking>
				                  </VideoClicks>
				                  
				                  <MediaFiles>
				                  	{{#each mediaFiles as |mediaFile|}}
				                     <MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				                  	{{/each}}
				                  	
				                  	{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
				                  </MediaFiles>

			                  {{/unless}}

			               </Linear>
			            </Creative>
			         </Creatives>
			         	<Extensions>
								<Extension type="waterfall" fallback_index="0"/>
								<Extension type="banner-installation">
					<![CDATA[ {{baseUrl}}?uid={{uuid}}&flightIdBIS={{bannerInstallFlightID}}&pid={{bannerInstalPmID}}&out=html&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&app_ver={{appVersion}}&ver={{sdkVersion}}&mdata={{mdata}}&ext={{extraData}}&deviceOsId={{deviceOsID}}&mac={{macAddress}}&pubUID={{publisherUID}} ]]>
				</Extension>
							</Extensions>
			         
			   {{#if adTagURI}}
			      </Wrapper>
			   {{/if}}
			   {{#unless adTagURI}}
			      </InLine>
			   {{/unless}}
			   </Ad>
			   			<Ad id="fallback_{{passbackData}}">
						<Wrapper>
							<AdSystem version="1.0">AdsPLAY</AdSystem>
							<AdTitle>
								<![CDATA[ AdsFallback]]>
							</AdTitle>
							<Description>
								<![CDATA[ AdsFallback_run]]>
							</Description>
							<VASTAdTagURI>
								<![CDATA[
{{baseUrl}}/delivery?pdata={{passbackData}}&placement={{placementId}}&pid={{placementId}}&url={{pageUrl}}&adFallBack=1&thirdptName=Falback-Ads&ua={{userAgent}}
]]>
							</VASTAdTagURI>
						
							<Extensions>
								<Extension type="waterfall" fallback_index="1"/>
							</Extensions>
						</Wrapper>
					</Ad>
			</VAST>
		</vmap:VASTAdData>
		</vmap:AdSource>
		{{/each}}
	
	</vmap:AdBreak>
	{{/each}}
 </vmap:VMAP>
