<?xml version="1.0" encoding="UTF-8"?>
<VAST version="3.0">

	{{#each instreamVideoAds}}
		<Ad id="{{adId}}">
			<InLine>
				<AdSystem version="1.0">AdsPLAY</AdSystem>
				<AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
				<Description>
							<![CDATA[{{description}}]]>
				</Description>
				  <Error>
						<![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=error&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&errorCode=[ERRORCODE]]]>
					 </Error>
					 
					 <Error>
						<![CDATA[ {{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]>
					 </Error>
					 
			         <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=impression&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Impression>
				
					 <Impression><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=request&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&isFallbackAd={{isFallbackAd}}]]></Impression>

				{{#each impression3rdUrls}}
					<Impression><![CDATA[{{this}}]]></Impression>
				{{/each}}

				<Creatives>
					<Creative sequence="1" id="{{adId}}">						
						<Linear skipoffset="{{skipoffset}}" starttime="{{startTime}}" >
							<Duration>{{duration}}</Duration>
							<TrackingEvents>
								{{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=creativeView&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played0&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played25&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played50&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played75&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=played100&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=close&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                  	 <Tracking event="pause"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=paused&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                  	 <Tracking event="mute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=mute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
			                  	 <Tracking event="unmute"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=unmute&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
								 <Tracking event="skip"><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=skip&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&imaCB=[TIMESTAMP]&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}]]></Tracking>
							</TrackingEvents>
							<VideoClicks>{{videoClicksTags}}
								<ClickTracking><![CDATA[{{logUrl}}?uid={{uuid}}&pubUID={{publisherUID}}&pubUID={{publisherUID}}&pid={{placementId}}&evt=click&waid={{webAppId}}&chid={{channelId}}&cid={{campaignId}}&adid={{adId}}&fid={{flighId}}&caid={{parentCategory}}&ip={{ipClient}}&ua={{userAgent}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&js={{jsSupport}}&plw={{playerWidth}}&plh={{playerHeight}}&scw={{screenWidth}}&sch={{screenHeight}}&cb={{time}}&weightKey={{weightKey}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&imaCB=[TIMESTAMP]]]></ClickTracking>
							</VideoClicks>
							<MediaFiles>
							{{#each mediaFiles as |mediaFile|}}							
								<MediaFile height="{{mediaFile.height}}" width="{{mediaFile.width}}" bitrate="{{mediaFile.bitrate}}" type="{{mediaFile.type}}" delivery="{{mediaFile.delivery}}"><![CDATA[{{mediaFile.mediaSource}}]]></MediaFile>
							{{/each}}
							
							{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
							</MediaFiles>
						</Linear>
					</Creative>					
				</Creatives>
			</InLine>
		</Ad>
	{{/each}}		
	
</VAST>
