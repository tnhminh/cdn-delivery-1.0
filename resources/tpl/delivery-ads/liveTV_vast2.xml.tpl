<?xml version="1.0" encoding="UTF-8"?>
<VAST version="2.0.1">

	{{#each instreamVideoAds}}
		<Ad id="{{adId}}">
			<InLine>
				<AdSystem version="1.0">AdsPLAY</AdSystem>
				<AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
				  <Error>
						<![CDATA[ {{logUrl}}?evt=error&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}&errorCode=[ERRORCODE]]]>
					 </Error>
			         <Impression><![CDATA[{{logUrl}}?evt=impression&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Impression>
				{{#each impression3rdUrls}}
					<Impression><![CDATA[{{this}}]]></Impression>
				{{/each}}

				<Creatives>
					<Creative sequence="1" id="{{adId}}">						
						<Linear starttime="{{startTime}}" >
							<Duration>{{duration}}</Duration>
							<TrackingEvents>
								{{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?evt=creativeView&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?evt=played0&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?evt=played25&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?evt=played50&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?evt=played75&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?evt=played100&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?evt=close&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
								 <Tracking event="pause"><![CDATA[{{logUrl}}?evt=paused&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                  	 <Tracking event="mute"><![CDATA[{{logUrl}}?evt=mute&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
			                  	 <Tracking event="unmute"><![CDATA[{{logUrl}}?evt=unmute&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></Tracking>
							</TrackingEvents>
							<VideoClicks>{{videoClicksTags}}
								<ClickTracking><![CDATA[{{logUrl}}?evt=click&adid={{adId}}&pid={{placementId}}&cid={{campaignId}}&fid={{flighId}}&channel={{channelName}}&timePushEvent={{timePushEvent}}&buyType={{buyType}}&pdata={{passbackData}}&midValue={{midValue}}&purl={{pageUrl}}&rurl={{refererUrl}}&app_ver={{appVersion}}&ver={{sdkVersion}}&ext={{extraData}}&t={{time}}&ad_type={{adType}}]]></ClickTracking>
							</VideoClicks>
							<MediaFiles>
							{{#each mediaFiles as |mediaFile|}}							
								<MediaFile height="{{mediaFile.height}}" width="{{mediaFile.width}}" bitrate="{{mediaFile.bitrate}}" type="{{mediaFile.type}}" delivery="{{mediaFile.delivery}}"><![CDATA[{{mediaFile.mediaSource}}]]></MediaFile>
							{{/each}}
							
							{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
							</MediaFiles>
						</Linear>
					</Creative>					
				</Creatives>
			</InLine>
		</Ad>
	{{/each}}		
	
</VAST>
