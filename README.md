AdsPlay - adsplay-delivery
====================
tnhminh075@gmail.com


Redis Configs deploy in PRODUCTION:

{
  "clusterInfoRedis": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ],
  "deliveryCache": [
    {
      "host": "118.69.190.41",
      "port": 6401
    }
  ],
  "creativeData": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ],
  "flightData": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ],
  "placementData": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ],
  "bookingData": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ],
  "flightWeightData": [
    {
      "host": "118.69.190.46",
      "port": 6480
    }
  ]
}

===================================================

Redis Config for Staging:
{
  "clusterInfoRedis": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ],
  "deliveryCache": [
    {
      "host": "118.69.190.41",
      "port": 6401
    }
  ],
  "creativeData": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ],
  "flightData": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ],
  "placementData": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ],
  "bookingData": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ],
  "flightWeightData": [
    {
      "host": "118.69.135.198",
      "port": 6379
    }
  ]
}